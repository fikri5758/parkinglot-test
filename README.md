#How to use this Project

Run the main program
"node index.js"

Inputs:
----------------------
create_parking_lot 6<br />
park KA-01-HH-1234<br />
park KA-01-HH-9999<br />
park KA-01-BB-0001<br />
park KA-01-HH-7777<br />
park KA-01-HH-2701<br />
park KA-01-HH-3141<br />
leave KA-01-HH-3141 4<br />
status<br />
park KA-01-P-333<br />
park DL-12-AA-9999<br />
leave KA-01-HH-1234 4<br />
leave KA-01-BB-0001 6<br />
leave DL-12-AA-9999 2<br />
park KA-09-HH-0987<br />
park CA-09-IO-1111<br />
park KA-09-HH-0123<br />
status<br />

Outputs: 
------------------
Created parking lot with 6 slots<br />
Allocated slot number: 1<br />
Allocated slot number: 2<br />
Allocated slot number: 3<br />
Allocated slot number: 4<br />
Allocated slot number: 5<br />
Allocated slot number: 6<br />
Registration number KA-01-HH-<br />
3141 with Slot Number 6 is free with Charge 30<br />
Slot No.  Registration No.<br />
1   KA-01-HH-1234<br />
2   KA-01-HH-9999<br />
3   KA-01-BB-0001<br />
4   KA-01-HH-7777<br />
5   KA-01-HH-2701<br />
Allocated slot number: 6<br />
Sorry, parking lot is full<br />
Registration number KA-01-HH-<br />
1234 with Slot Number 1 is free with Charge 30<br />
Registration number KA-01-BB-<br />
0001 with Slot Number 3 is free with Charge 50<br />
Registration number DL-12-AA-9999 not found<br />
Allocated slot number: 1<br />
Allocated slot number: 3<br />
Sorry, parking lot is full<br />
Slot No.  Registration No.<br />
1   KA-09-HH-0987<br />
2   KA-01-HH-9999<br />
3   CA-09-IO-1111<br />
4   KA-01-HH-7777<br />
5   KA-01-HH-2701<br />
6   KA-01-P-333<br />