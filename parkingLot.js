let Car = [];
let maxSize = 0;
let availableSlot = [];
const utilFunctions = require('./func/utils');


let createParkingLot = async(noOfLot) => {
    try {
        maxSize = parseInt(noOfLot);
    } catch (e) {
        return "Parameter is not a number!";
    }

    for (let i = 1; i <= maxSize; i++) {
        availableSlot.push(i);
    }
    return `Created a parking lot with ${availableSlot.length} slots`;

}


let park = async(registratonNo) => {
    if (maxSize === 0) {
        return `parking lot is not initiated`;
    } else if (maxSize === Car.length) {
        return `Sorry, parking lot is full`;
    } else {
        let slot = availableSlot[0];
        Car.push({
            'slot': slot,
            'registratonNo': registratonNo
        });
        // console.log("pushed to car",Car);
        availableSlot.shift();
        return `Allocated slot number: ${slot}`
    }
}


let leave = async(registratonNo, hours) => {
    slot = parseInt(0);
    if (maxSize === 0) {
        return "parking lot is not initiated";
    } else if (Car.length > 0) {

        if (await utilFunctions.search(registratonNo, 'registratonNo', Car)) {
            // console.log(Car)
            // n = 4
            bayar = ((hours - 2) > 0) ? ((hours - 2) * 10 + 10) : 10
            slot = await getSlotNumberFromRegNo(registratonNo)
            Car = await utilFunctions.remove(registratonNo, 'registratonNo', Car);

            // Add a the number back into slot 
            availableSlot.push({
                'registratonNo': registratonNo,
                'hours': hours
            });
            availableSlot.sort();
            return `Slot  number ${slot} is free with Charge ${bayar}`;

        } else {
            return ` Slot ${slot} is already empty `;
        }

        console.log('Car ==>', Car);

    } else {
        return `Parking lot is empty`
    }
}


let status = async() => {
    if (maxSize === 0) {
        return "parking lot is not initiated";
    } else if (Car.length > 0) {

        console.log("Slot No.\tRegistration No.");
        Car.forEach(function(row) {
            console.log(row.slot + "\t         " + row.registratonNo);
        });

    } else {
        return `Parking lot is empty`
    }

}


let getSlotNumberFromRegNo = async(registratonNo) => {
    if (maxSize === 0) {
        return "parking lot is not initiated";
    } else if (Car.length > 0) {
        let resultSet;
        Car.forEach(function(row) {
            if (row.registratonNo === registratonNo) {
                resultSet = row.slot;
            }
        });
        if (resultSet === undefined) return `Not found`
        return resultSet;
    } else {
        return `Not found`
    }
}


module.exports = {
    createParkingLot,
    park,
    leave,
    status,
    getSlotNumberFromRegNo,
}